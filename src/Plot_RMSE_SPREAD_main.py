import sys
sys.modules[__name__].__dict__.clear()
# ================================ Importing the functions =====================================
## TODO: check the code and make it fancier!!!
from CCLM_OUTS import Plot_CCLM
import matplotlib.pyplot as plt
from RMSE_MAPS_INGO import read_data_from_mistral as rdfm
from sklearn.metrics import mean_squared_error
import numpy as np
import cartopy.crs as ccrs
from numpy import genfromtxt
import os, glob
# ==============================================================================================
# ================================== functions =================================================
def plot_rmse_spread(PDF="name.pdf",vari="RMSE",VAL=np.zeros((10,10,10)),x=10,y=10,col = ['k', 'r', 'b', 'g', 'm']):
    Plot_CCLM(dir_mistral='/work/bb1029/b324045/work5/03/member_relax_3_big_00/post/', name=name_2, bcolor='black',
              var=Vari, flag='FALSE', color_map='TRUE', alph=1, grids='FALSE', grids_color='red', rand_obs='FALSE',
              NN=NN)


    if vari == "RMSE":
        v = np.linspace(0, .8, 9, endpoint=True)
    else:
        v = np.linspace(0, .8, 9, endpoint=True)

    if vari == "RMSE":
        cs = plt.contourf(lons_f1,lats_f1,VAL, v, transform=ccrs.PlateCarree(), cmap=plt.cm.terrain)
        cb = plt.colorbar(cs)
        cb.set_label('RMSE [K]', fontsize=20)
        cb.ax.tick_params(labelsize=20)
    else:
        if vari == "SPREAD":
            cs = plt.contourf(lons_f1, lats_f1, VAL, v, transform=ccrs.PlateCarree(), cmap=plt.cm.terrain)
            cb = plt.colorbar(cs)
            cb.set_label('SPREAD [K]', fontsize=20)
            cb.ax.tick_params(labelsize=20)


    plt.hlines(y=min(rlat_o[buffer:-buffer]), xmin=min(rlon_o[buffer:-buffer]), xmax=max(rlon_o[buffer:-buffer]),
               color='black', linewidth=4)
    plt.hlines(y=max(rlat_o[buffer:-buffer]), xmin=min(rlon_o[buffer:-buffer]), xmax=max(rlon_o[buffer:-buffer]),
               color='black', linewidth=4)
    plt.vlines(x=min(rlon_o[buffer:-buffer]), ymin=min(rlat_o[buffer:-buffer]), ymax=max(rlat_o[buffer:-buffer]),
               color='black', linewidth=4)
    plt.vlines(x=max(rlon_o[buffer:-buffer]), ymin=min(rlat_o[buffer:-buffer]), ymax=max(rlat_o[buffer:-buffer]),
               color='black', linewidth=4)
    plt.savefig(PDF)
    plt.close()


# ==============================================================================================
fig = plt.figure('1')
fig.set_size_inches(14, 10)
# ================================= NAMELIST ===================================================
NN=1000
SEAS='DJF'
#SEAS='JJA'
Vari   = 'T_2M'
#Vari   = 'TOT_PREC'
buffer    = 20
name_2 = 'member_relax_3_big_00_' + Vari + '_ts_splitseas_1990_1999_' + SEAS + '.nc'
PDF1    = 'ENSEMBLE_RMSE_' + SEAS + '_'+ Vari +'.pdf'
PDF2    = 'ENSEMBLE_SPREAD_' + SEAS + '_'+ Vari +'.pdf'
timesteps=10   # number of the seasons (years)
start_time=0
t_o, lat_o, lon_o, rlat_o, rlon_o =rdfm(dir='/work/bb1029/b324045/work5/03/member_relax_3_big_00/post/', # the observation (default run without shifting)
                                            name=name_2,
                                            var=Vari)

here = "/home/fallah/Documents/DATA_ASSIMILATION/Bijan/CODES/CCLM/Python_Codes/historical_runs_yearly_ensemble/src/"
# ==============================================================================================


counter = 0
for kk in range(1,6):
    shift = kk

    for i in range(1,5):

        direc  = i
        if i == 3:                                           # northwest shift
            start_lon = (buffer + shift)
            start_lat = (buffer - shift)
        else:
            if i == 2:                                      # northeast shift
                start_lon = (buffer - shift)
                start_lat = (buffer - shift)
            else:
                if i == 1:                                  # southeast shift
                    start_lon = (buffer - shift)
                    start_lat = (buffer + shift)
                else:
                    start_lon = (buffer + shift)           # southwest shift
                    start_lat = (buffer + shift)


        dext_lon = t_o.shape[2] - (2 * buffer)
        dext_lat = t_o.shape[1] - (2 * buffer)

        name_1 = 'member_relax_'+ str(direc) +'_big_0' + str(shift) +'_'+ Vari +'_ts_splitseas_1990_1999_' + SEAS + '.nc'


        # ==============================================================================================
        t_f, lat_f, lon_f, rlat_f, rlon_f =rdfm(dir='/work/bb1029/b324045/work5/0'+ str(direc) +'/member_relax_' + str(direc) + '_big_0' + str(shift) + '/post/',
                                                name=name_1,
                                                var=Vari)


        forecast = t_f[start_time:timesteps, start_lat:start_lat + dext_lat, start_lon:start_lon + dext_lon]
        obs = t_o[start_time:timesteps, buffer:buffer + dext_lat, buffer:buffer + dext_lon]
        RMSE=np.zeros((forecast.shape[1], forecast.shape[2]))
        lats_f1=lat_f[start_lat:start_lat + dext_lat, start_lon:start_lon + dext_lon]
        lons_f1=lon_f[start_lat:start_lat + dext_lat, start_lon:start_lon + dext_lon]
        #print forecast.shape
        for ii in range(0,forecast.shape[1]):
            for jj in range(0,forecast.shape[2]):
                forecast_resh=np.squeeze(forecast[:,ii,jj])
                obs_resh=np.squeeze(obs[:,ii,jj])
                RMSE[ii,jj] = mean_squared_error(obs_resh, forecast_resh) ** 0.5


        np.savetxt("RMSE_"+str(counter)+".csv", RMSE, delimiter=",")
        for rr in range(0,timesteps-start_time):
            np.savetxt("SPREAD_"+ str(rr) + str(counter)+".csv", np.squeeze(forecast[rr,:,:]), delimiter=",")

        counter = counter + 1




RMSE = np.zeros((forecast.shape[1],forecast.shape[2]))


for pp in range(0, 20):

    RMSE = RMSE + genfromtxt("RMSE_" + str(pp) + ".csv", delimiter=',')

RMSE = RMSE / 20
SPREAD = np.zeros((20,forecast.shape[1],forecast.shape[2]))
SPREAD_final = np.zeros((forecast.shape[1],forecast.shape[2]))
dumm = np.zeros((forecast.shape[1],forecast.shape[2]))
summ = np.zeros((timesteps,forecast.shape[1],forecast.shape[2]))
for gg in range(0,timesteps-start_time): # finding the standard deviation between all members over each time and then averaging them over time:
    for dd in range(0,20):
        dumm = genfromtxt("SPREAD_"+ str(gg) + str(dd)+".csv", delimiter=',')
        SPREAD[dd,:,:] = np.squeeze(dumm[:,:])
    summ[gg,:,:] = np.std(SPREAD,0)
SPREAD_final = np.mean(summ,0)


# PLOTING ====================================================================================================================

plot_rmse_spread(PDF=PDF1,vari="RMSE",VAL=RMSE,x=forecast.shape[1],y=forecast.shape[2])
plot_rmse_spread(PDF=PDF2,vari="SPREAD",VAL=SPREAD_final,x=forecast.shape[1],y=forecast.shape[2])


for filename in glob.glob(here + "TEMP/SPREAD_*.csv"):
    os.remove(filename)


for filename in glob.glob(here + "TEMP/RMSE_*.csv"):
    os.remove(filename)















### http://eca.knmi.nl/download/ensembles/Haylock_et_al_2008.pdf